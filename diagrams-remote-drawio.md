# Remote Diagrams in Markdown

[[_TOC_]]

## Overview

[Draw.io](https://www.draw.io/) is an online diagram editor for making flowcharts, process diagrams, org charts, UML, ER and network diagrams.

In contrast to PlantUML or Mermaid, Draw.io diagrams are not created from a plain plain text language. Draw.io diagrams are created with a graphical online editor.

## GitLab

As a consecuence Draw.io diagrams cannot be defined in Markdown and in GitLab directly.

However, Draw.io supports GitLab as storage backend. This enables Draw.io to create new diagrams in a GitLab repository and also to update existing diagrams.

### Create a new Draw.io Diagram

New diagrams can be created with the [Draw.io online editor](https://www.draw.io/?mode=gitlab). They can be saved in a GitLab repository of a linked account.

Draw.io supports multiple storage formats. The default is the `.drawio` format, which is based on XML. However, this format should not be used, beceause Markdown is not able to render such diagrams.

Fortunately, Draw.io supports an extended `png` and `svg` storage format, which can be displayed like noraml `png` and `svg` files, but also contains additional information that is required by Draw.io to edit them.

### Link an existing Draw.io Diagram

A diagram created by Draw.io and which has been stored as `png` or `svg` file can be included in a Markdown document like any other image:

```makdowm
![Diagram](drawio-diagram.png)
```

![Diagram](drawio-diagram.png)

[Edit](https://www.draw.io/?mode=gitlab#Aueisele-playground%2Fdoc%2Fmaster%2Fdrawio-diagram.png) | [Edit As New](https://www.draw.io/#Uhttps%3A%2F%2Fgitlab.com%2Fueisele-playground%2Fdoc%2F-%2Fraw%2Fmaster%2Fdrawio-diagram.png)

Because the `png` (or `svg`) file also contains extended information, Draw.io can also edit this image file.

The following Markdown snippet creates a link to open [drawio-diagram.png](drawio-diagram.png) in edit mode:

```markdown
[Edit](https://www.draw.io/?mode=gitlab#Aueisele-playground%2Fdoc%2Fmaster%2Fdrawio-diagram.png)
```

The following Markdown snippet creates a link to open [drawio-diagram.png](drawio-diagram.png) as a new diagram:

```markdown
[Edit As New](https://www.draw.io/#Uhttps%3A%2F%2Fgitlab.com%2Fueisele-playground%2Fdoc%2F-%2Fraw%2Fmaster%2Fdrawio-diagram.png)
```
